#include <AFMotor.h>
#include <Servo.h> 
#include <NewPing.h>

#define TRIG_PIN A4 
#define ECHO_PIN A5 
#define MAX_DISTANCE 400
#define MAX_SPEED 200 
#define MAX_SPEED_OFFSET 10 
#define COLL_DIST 15 
#define TURN_DIST COLL_DIST+10
NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE); 

AF_DCMotor motor1(3, MOTOR12_1KHZ);   //Rechts
AF_DCMotor motor2(4, MOTOR12_1KHZ);   //Links
Servo myservo;  

int pos = 0; 
int maxDist = 0;
int maxAngle = 0;
int maxRight = 0;
int maxLeft = 0;
int maxFront = 0;
int course = 0;
int curDist = 0;
String motorSet = "";
boolean goesForward=false;
int distance = 100;
int speedSet = 0;

void setup() {
  // initialize serial communication:
  Serial.begin(9600);

  myservo.attach(10);
  myservo.write(90);
  checkPath();
  motorSet = "FORWARD";
  myservo.write(90);
  moveForward(); 

}
//------------------------------------------------------------------------------------------------------------------------------------

//---------------------------------------------MAIN LOOP ------------------------------------------------------------------------------
void loop() {
   if (curDist >= TURN_DIST) {
    Serial.print(curDist);
    Serial.println(":curDist loop");
    motorSet="FORWARD";
    checkForward();
   }   
  checkPath();  
}
//-------------------------------------------------------------------------------------------------------------------------------------
void checkPath() {
  int curLeft = 0;
  int curFront = 0;
  int curRight = 0;
  int curDist = 0;
  
  Serial.println("Begin *** CheckPath");

  //myservo.write(144);
  Serial.println("myservo set to : 90"); 
  curDist = readPing();
  if (curDist > TURN_DIST) {
    motorSet="FORWARD";
    checkForward();
  }  
  else {
    for(pos = 144; pos >= 36; pos-=18) 
    {
      myservo.write(pos); 
      delay(90); 
      checkForward(); 
      curDist = readPing();
      Serial.print(curDist); 
      Serial.println("   :curDist  else");

      if (curDist < COLL_DIST) {
        //- COLL_DIST=15
        Serial.print(curDist); 
        Serial.println("   :curDist < COLL_DIST in der for schleife");
        checkCourse(); 
        break; 
      }
    
      if (curDist < TURN_DIST) { 
        //- TURN_DIST COLL_DIST+10
        changePath(); 
        Serial.println("back from changePath ");
      }
      if (curDist > maxDist) {maxDist = curDist; maxAngle = pos; Serial.println("set maxDist");}
      if (pos > 90 && curDist > curLeft) { curLeft = curDist;}
      if (pos == 90 && curDist > curFront) {curFront = curDist;}
      if (pos < 90 && curDist > curRight) {curRight = curDist;}
    }
   maxLeft = curLeft;
   maxRight = curRight;
   maxFront = curFront;
 }
}
//-------------------------------------------------------------------------------------------------------------------------------------
void setCourse() { 
    Serial.print(maxAngle); 
    Serial.println("   :maxAngle");
    if (maxAngle < 90) {turnRight();}
    if (maxAngle > 90) {turnLeft();}
    maxLeft = 0;
    maxRight = 0;
    maxFront = 0;
}
//-------------------------------------------------------------------------------------------------------------------------------------
void checkCourse() { 
  moveBackward();
  delay(500);
  moveStop();
  setCourse();
}
//-------------------------------------------------------------------------------------------------------------------------------------
void changePath() {
  Serial.print(pos);
  Serial.println("   :pos in changePath()");
  if (pos < 90) {veerLeft(); } 
  if (pos > 90) {veerRight(); } 
}
//-------------------------------------------------------------------------------------------------------------------------------------

int readPing() { 
  delay(70);
  unsigned int uS = sonar.ping();
  int cm = uS/US_ROUNDTRIP_CM;
  return cm;
}
//-------------------------------------------------------------------------------------------------------------------------------------
void checkForward() { if (motorSet=="FORWARD") {motor1.run(BACKWARD); motor2.run(BACKWARD); } }     
//-------------------------------------------------------------------------------------------------------------------------------------
void checkBackward() { if (motorSet=="BACKWARD") {motor1.run(FORWARD); motor2.run(FORWARD); } } 
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
void moveStop() {motor1.run(RELEASE); motor2.run(RELEASE);}  
//-------------------------------------------------------------------------------------------------------------------------------------
void moveForward() {
    motorSet = "FORWARD";
    motor1.run(BACKWARD);      
    motor2.run(BACKWARD); 
     
  for (speedSet = 0; speedSet < MAX_SPEED; speedSet +=2) 
  {
    motor1.setSpeed(speedSet+MAX_SPEED_OFFSET);
    motor2.setSpeed(speedSet);
    delay(5);
  } 
}
//-------------------------------------------------------------------------------------------------------------------------------------
void moveBackward() {
    motorSet = "BACKWARD";
    motor1.run(FORWARD);      
    motor2.run(FORWARD);     
  for (speedSet = 0; speedSet < MAX_SPEED; speedSet +=2) 
  {
    motor1.setSpeed(speedSet+MAX_SPEED_OFFSET);
    motor2.setSpeed(speedSet);
    delay(5);
  }
}  
//-------------------------------------------------------------------------------------------------------------------------------------
void turnRight() {
  motorSet = "RIGHT";
  motor1.run(FORWARD);     
  motor2.run(BACKWARD);     
  delay(400); 
  motorSet = "FORWARD";
  motor1.run(BACKWARD);      
  motor2.run(BACKWARD);      
}  
//-------------------------------------------------------------------------------------------------------------------------------------
void turnLeft() {
  motorSet = "LEFT";
  motor1.run(BACKWARD);     
  motor2.run(FORWARD);      
  delay(400); 
  motorSet = "FORWARD";
  motor1.run(BACKWARD);      
  motor2.run(BACKWARD);     
}  
//-------------------------------------------------------------------------------------------------------------------------------------
void veerRight() {motor2.run(FORWARD); delay(400); motor2.run(BACKWARD); Serial.println("*** VEERRight");} 
//-------------------------------------------------------------------------------------------------------------------------------------
void veerLeft() {motor1.run(FORWARD); delay(400); motor1.run(BACKWARD); Serial.println("*** VEERLeft");} 
//-------------------------------------------------------------------------------------------------------------------------------------
